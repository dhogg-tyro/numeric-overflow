package app;

import java.math.BigInteger;
import java.util.Arrays;

public class Main {

    private int threshold = 1000;

    public static void main(String[] args) throws Exception {

        Main app = new Main();

        int amount = 999;
        if (!app.approval(BigInteger.valueOf(amount))) {
            System.out.println(amount + " does not require approval");
        }

        amount = 2000;
        if (app.approval(BigInteger.valueOf(amount))) {
            System.out.println(amount + " requires approval");
        }
    }

    public boolean approval(BigInteger amount) throws Exception {
        return typedApproval(new Amount(amount));
    }

    private boolean typedApproval(Amount amount) {
        if(amount.getAmount() >= threshold) {
            return true;
        }
        return false;
    }

}
