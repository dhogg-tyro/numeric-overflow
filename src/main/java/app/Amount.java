package app;

import java.math.BigInteger;

public class Amount {
    private final Integer amount;
    private final Integer max_amount = 1000000;  // Business defined max value
    private final Integer min_amount = 0;        // Business defined min value

    public Amount(final BigInteger amount) throws Exception {
        validate(amount);
        this.amount = amount.intValue();
    }

    public Integer getAmount() {
        return this.amount;
    }

    private void validate(final BigInteger amount) throws Exception {
        if (amount.compareTo(BigInteger.valueOf(min_amount)) < 0) {
            throw new Exception("amount is less than " + min_amount.toString());
        }

        if (amount.compareTo(BigInteger.valueOf(max_amount)) > 0) {
            throw new Exception("amount is greater than " + max_amount.toString());
        }
    }
}
