package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

@DisplayName("Usability unit tests")
public class appSpec {

    @Test
    public void ThousandsNeedsApproval() {
        try {
            Main app = new Main();
            boolean res = app.approval(BigInteger.valueOf(1000));
            assertTrue(res,() -> "1000 needs approval");
        } catch (Exception ex) {
        }
    }

    @Test
    public void FileHundredsDoesNotNeedApproval() {
        try {
            Main app = new Main();
            boolean res = app.approval(BigInteger.valueOf(500));
            assertFalse(res,() -> "500 does not need approval");
        } catch (Exception ex) {
        }
    }

}
