package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Tag;

import java.math.BigInteger;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Security unit tests")
@Tag("security")
public class appSecuritySpec {

    @Test
    public void BiggerThanIntMaxSizeNeedsApproval() {

        try {
            Main app = new Main();
            boolean res = app.approval(BigInteger.valueOf(2147483647+1));
            assertTrue(res,() -> "Bigger than int max size needs approval");
        } catch (Exception ex) {
        }

    }

    @Test
    public void LessThanIntMinSizeNeedsApproval() {

        try {
            Main app = new Main();
            boolean res = app.approval(BigInteger.valueOf(-2147483648-1));
            assertTrue(res,() -> "Less than int min size needs approval");
        } catch (Exception ex) {
        }

    }
}
